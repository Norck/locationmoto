<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Location moto Soa, nos differentes catégories de motos à louer avec facilité de payement à Analakely</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/minified.css" type="text/css">
    <script src="js/minified.js" defer></script>
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        locationsoa@gmail.com
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        032 45 654 67	| 034 46 574 58
                    </div>
                </div>
                <div class="ht-right">
						<?php if(isset($_SESSION['nomUtilisateur'])){ ?>
								<a href="page/logout.php" class="login-panel"><i class="fa fa-user"></i><?php echo($_SESSION['nomUtilisateur'] . ''); ?> Se déconnecter</a>
						<?php }else{ ?>
							<a href="login.html" class="login-panel"><i class="fa fa-user"></i>Se connecter</a>
						<?php } ?>
						
					
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner-header">
				<div class="row">
                    <div class="">
                        <div class="logo">
                            <h1 style="color:#FFCF64;font-size:25px;">Location moto Soa Analakely</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="advanced-search">
                            <button type="button" class="category-btn">All Categories</button>
                            <div class="input-group">
                                <input type="text" placeholder="What do you need?">
                                <button type="button"><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 text-right col-md-3">
                        <ul class="nav-right">
                            <li class="heart-icon">
								
                            </li>
                            <li class="cart-icon">
                                
                            </li>
                            <li class="cart-price"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <li class="active"><a href="./index.html"><h4>Home</h4></a></li>
                        <li><a href="#"><h4>Catégorie</h4></a>
                            <ul class="dropdown">
                                <li><a href="categorie-categM2.html">Roadster</a></li>
                                <li><a href="categorie-categM1.html">Routière</a></li>
                                <li><a href="categorie-categM3.html">Tout-terrain</a></li>
                            </ul>
                        </li>
                        <li><a href="./promotion.html"><h4>Promotions</h4></a></li>
                        <li><a href="./contact.html"><h4>Contact</h4></a></li>
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Banner Section Begin -->
    <div class="banner-section spad">
			
			<h1>A propos</h1>
			<br>
			<p>C’est connu de tous que le nombre de <strong>2 roues</strong> prennent de l’importance par rapport aux voitures à Madagascar. En effet, ils peuvent être économiquement profitable et régler les problèmes d’embouteillage ou offrent d’autres expériences en dehors des villes.</p>
			<p>C’est pourquoi notre existence en tant que <strong>location de moto</strong> situé à <strong>Antananarivo</strong> dans le quartier d’ <strong>Analakely</strong> là où vous pouvez acquérir pendant quelques jours un engin que vous n’auriez jamais cru pouvoir conduire.</p>
			<p>Nous vous offrons un large possibilité de choix qui devraient répondre à vos besoins d’où nos catégories de motos disponibles. Comme catégorie, on offre du <a href= "categorie.php?idCategorie=categM2"><strong>roadster</strong> </a>: une bonne machine faite pour les villes et les routes nationales, offrant une dose de puissance avec le minimum de confort, peut être considéré comme moto de course bon marché. Ensuite, nous avons du du <a href= "categorie.php?idCategorie=categM1"><strong>routière</strong></a>, étant très massive et offrant le plus de confort de conduite même sur de longs trajets, la locations étant <strong>pas chère</strong> comparée à son prix de vente. Aussi, il y a le du <a href= "categorie.php?idCategorie=categM3"><strong>tout-terrain</strong></a>, faite pour les routes secondaires, recommandé pour ceux qui souhaitent s’initier au plaisir de la sauvagerie sur piste ou amateurs de sports extrêmes .
			<p>Mais le plus important est notre facilité de paiement qui vous permet de payer vos factures avec BNI Madagascar ou Airtel Money ou MVola . La possibilité d’un paiement multiple est aussi rendu possible après un certains nombre d’achats de services. A part, nous faisons occasionnellement des promotions pour la location de certains produits selon les périodes de l’année alors n’hésitez pas à Location Soa .

			
			<h2>Les catégories de moto à louer</h2>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="img/moto-roadster-suzuki-gsx-s1000.jpg" alt="moto-roadster-suzuki-gsx-s1000">
                        <div class="inner-text">
                            <a href="./categorie-categM2.html"><h3>Roadster</h3></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="img/moto-routiere-bmw-k1600-gt.jpg" alt="moto-routiere-bmw-k1600-gt"></a>
                        <div class="inner-text">
                            <a href="categorie-categM1.html"><h3>Routière</h3></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="img/moto-tout-terrain-yamaha-YZ250F.jpg" alt="moto-tout-terrain-yamaha-YZ250F">
                        <div class="inner-text">
                            <a href="categorie-categM3.html"><h3>Tout-terrain</h3></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Section End -->

	
    <!-- Partner Logo Section Begin -->
    <div class="partner-logo">
        <div class="container">
            <div class="logo-carousel owl-carousel">
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/bni-madagascar.jpg" alt="bni madagascar">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/airtel-money.jpg" alt="airtel money">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/m-vola.jpg" alt="m vola">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partner Logo Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

</body>

</html>

