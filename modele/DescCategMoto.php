<?php
	class DescCategMoto{
		private $idCategorie;
		private $titre;
		private $description;
		private $pageTitle;
		private $meta;
		
		
		public function getPageTitle(){
			return $this->pageTitle;
		}
		public function setPageTitle($pageTitle){
			$this->pageTitle = $pageTitle;
		}
		
		public function getMeta(){
			return $this->meta;
		}
		public function setMeta($meta){
			$this->meta = $meta;
		}
		
		public function getIdCategorie(){
			return $this->idCategorie;
		}
		public function setIdCategorie($idCategorie){
			$this->idCategorie = $idCategorie;
		}
		public function getTitre(){
			return $this->titre;
		}
		public function setTitre($titre){
			$this->titre = $titre;
		}
		public function getDescription(){
			return $this->description;
		}
		public function setDescription($description){
			$this->description = $description;
		}
		
		public function __construct($idCategorie, $titre, $description, $pageTitle, $meta){
			$this->idCategorie = $idCategorie;
			$this->titre = $titre;
			$this->description = $description;
			$this->pageTitle = $pageTitle;
			$this->meta = $meta;
		}
	}
?>	
