<?php
	class Moto{
		
		private $id;
		private $idModele;
		private $modele;
		private $idMarque;
		private $marque;
		private $tarifs;//tableau
		private $description;
		private $idCategorie;
		private $categorie;
		private $cylindree;
		private $photos;//tableau
		
		public function getId(){
			return $this->name;
		}
		public function setId($id){
			$this->id = $id;
		}
		public function getIdModele(){
			return $this->idModele;
		}
		public function setIdModele($idModele){
			$this->idModele = $idModele;
		}
		public function getModele(){
			return $this->modele;
		}
		public function setModele($modele){
			$this->modele = $modele;
		}
		public function getIdMarque(){
			return $this->idMarque;
		}
		public function setIdMarque($idMarque){
			$this->idMarque = $idMarque;
		}
		public function getMarque(){
			return $this->marque;
		}
		public function setMarque($marque){
			$this->marque = $marque;
		}
		public function getTarifs(){
			return $this->tarifs;
		}
		public function setTarifs($tarifs){
			$this->tarifs = $tarifs;
		}
		public function getDescription(){
			return $this->description;
		}
		public function setDescription($description){
			$this->description = $description;
		}
		public function getIdCategorie(){
			return $this->idCategorie;
		}
		public function setIdCategorie($idCategorie){
			$this->idCategorie = $idCategorie;
		}
		public function getCategorie(){
			return $this->categorie;
		}
		public function setCategorie($categorie){
			$this->categorie = $categorie;
		}
		public function getCylindree(){
			return $this->cylindree;
		}
		public function setCylindree($cylindree){
			$this->cylindree = $cylindree;
		}
		public function getPhotos(){
			return $this->photos;
		}
		public function setPhotos($photos){
			$this->photos = $photos;
		}
		
		public function __construct($id, $idModele, $modele, $idMarque, $marque, $tarifs, $description, $idCategorie, $categorie, $cylindree, $photos){
			$this->id = $id;
			$this->idModele = $idModele;
			$this->modele = $modele;
			$this->idMarque = $idMarque;
			$this->marque = $marque;
			$this->tarifs = $tarifs;
			$this->description = $description;
			$this->idCategorie = $idCategorie;
			$this->categorie = $categorie;
			$this->cylindree = $cylindree;
			$this->photos = $photos;
		}
		
	}
?>
