<?php
	class Tarif{
		
		private $id;
		private $idModele;
		private $nbJour;
		private $prix;
		private $caution;
		
		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id = $id;
		}
		public function getIdModele(){
			return $this->idModele;
		}
		public function setIdModele($idModele){
			$this->idModele = $idModele;
		}
		public function getNbJour(){
			return $this->nbJour;
		}
		public function setNbJour($nbJour){
			$this->nbJour = $nbJour;
		}
		public function getPrix(){
			return $this->prix;
		}
		public function setPrix($prix){
			$this->prix = $prix;
		}
		public function getCaution(){
			return $this->caution;
		}
		public function setCaution($caution){
			$this->caution = $caution;
		}
		
		public function __construct($id, $idModele, $nbJour, $prix, $caution){
			$this->id = $id;
			$this->idModele = $idModele;
			$this->nbJour = $nbJour;
			$this->prix = $prix;
			$this->caution = $caution;
		}
	}
?>
