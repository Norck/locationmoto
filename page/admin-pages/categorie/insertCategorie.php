<?php 
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include_once('./../../../modele/Moto.php');
	include_once('./../../../modele/DescCategMoto.php');
	include_once('./../../../service/Connection.php');
	include_once('./../../../service/Categorie.php');
	include('./../../../service/tools.php');
	
	$successInsertNom = null;
	$errorInsertNom = null;
	
	$successUpdateNom = null;
	$errorUpdateNom = null;
	
	$successDeleteNom = null;
	$errorDeleteNom = null;
	
	$successInsertDesc = null;
	$errorInsertDesc = null;
	
	$successUpdateDesc = null;
	$errorUpdateDesc = null;
	
	$successDeleteDesc = null;
	$errorDeleteDesc = null;
	
	$pdo = getPdo();
	
	//INSERT NOM
	if(isset($_POST['insertNom'])){
		
		$insertNom = $_POST['insertNom'];
		$insertNom = strReplace($insertNom);
		
		$success = insererNomCategorie($pdo, $insertNom);
		if($success == 1){
			$successInsertNom = "Insertion du nom réussite.";
		}else{
			$errorInsertNom = "Nom déjà existant.";
		}
	}
	
	//UPDATE NOM
	if(isset($_POST['updateNomCategorie']) && isset($_POST['updateIdNom'])){
		
		$updateNomCategorie = $_POST['updateNomCategorie'];
		$updateIdNom = $_POST['updateIdNom'];
		
		$updateNomCategorie = strReplace($updateNomCategorie);
		$updateIdNom = strReplace($updateIdNom);
		
		$success = updateNomCategorie($pdo, $updateIdNom, $updateNomCategorie);
		if($success == 1){
			$successUpdateNom = "Mise à jour du nom réussit.";
		}else{
			$errorUpdateNom = "Echec de la mise à jour du nom.";
		}
	}
	
	//DELETE CATEGORIE
	if(isset($_POST['deleteIdNom'])){
		$deleteIdNom = $_POST['deleteIdNom'];
		$deleteIdNom = strReplace($deleteIdNom);
		try{
			deleteCategorie($pdo, $deleteIdNom);
			$successDeleteNom = "Suppression de la catégorie réussite.";
		}catch(PDOException $e){
			$errorDeleteNom = $e->getMessage();
		}
	}
	
	//INSERT DESCRIPTION
	if(isset($_POST['insertDescIdCateg']) && isset($_POST['titre']) && isset($_POST['description'])){
		
		$insertDescIdCateg = $_POST['insertDescIdCateg'];
		$titre = $_POST['titre'];
		$description = $_POST['description'];
		
		$insertDescIdCateg = strReplace($insertDescIdCateg);
		$titre = strReplace($titre);
		$description = strReplace($description);
		
		$success = insertDescription($pdo, $insertDescIdCateg, $titre, $description);
		if($success == 1){
			$successInsertDesc = "Insertion de la description réussite.";
		}else{
			$errorInsertDesc = "Insertion de la description échouée.";
		}
	}
	
	//UPDATE DESCRIPTION
	if(isset($_POST['updateDescIdCateg']) && isset($_POST['titre']) && isset($_POST['description'])){
		
		$updateDescIdCateg = $_POST['updateDescIdCateg'];
		$titre = $_POST['titre'];
		$description = $_POST['description'];
		
		$updateDescIdCateg = strReplace($updateDescIdCateg);
		$titre = strReplace($titre);
		$description = strReplace($description);
		
		$success = updateDescription($pdo, $updateDescIdCateg, $titre, $description);
		if($success == 1){
			$successUpdateDesc = "Mise à jour de la description réussite.";
		}else{
			$errorUpdateDesc = "Mise à jour de la description échouée.";
		}
	}
	
	//DELETE DESCRIPTION
	if(isset($_POST['deleteDescIdCateg'])){
		$success = deleteDescription($pdo, $_POST['deleteDescIdCateg']);
		if($success == 1){
			$successDeleteDesc = "Suppression de la description réussite.";
		}else{
			$errorDeleteDesc = "Suppression de la description échouée.";
		}
	}
	
	$categories = getAllCategories($pdo);

?>
<?php include('../header.php'); ?>
<button class=" admin-back-button" ><a href="../../back.html">Précédant</a></button>
<h2>Nom</h2>
<h3>Inserer nouveau nom catégorie</h3>
<form action="categorie.html" method="post">
	<?php if($successInsertNom != null){ ?>
		<div class="successBackAdmin"><p><?php echo($successInsertNom); ?></p></div>
	<?php } ?>
	<?php if($errorInsertNom != null){ ?>
		<div class="errorBackAdmin"><p><?php echo($errorInsertNom); ?></p></div>
	<?php } ?>
	<p><label>Nom catégorie</label>
	<input type="text" name="insertNom" placeholder="sportive"></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>
<br>

<h3>Mettre à jour nom catégorie</h3>
<?php if($successUpdateNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successUpdateNom); ?></p></div>
<?php } ?>
<?php if($errorUpdateNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorUpdateNom); ?></p></div>
<?php } ?>
<form action="categorie.html" method="post">
	<p><label>Nom actuel</label>
	<select name="updateIdNom">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Nouveau nom</label>
	<input type="text" name="updateNomCategorie" placeholder="sportive"></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>
<br>

<h3>Supprimer une catégorie</h3>
<?php if($successDeleteNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successDeleteNom); ?></p></div>
<?php } ?>
<?php if($errorDeleteNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorDeleteNom); ?></p></div>
<?php } ?>
<form action="categorie.html" method="post">
	<p><label>Nom</label>
	<select name="deleteIdNom">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>
<br>


<h2>Description</h2>


<h3>Insérer une description</h3>

<?php if($successInsertDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successInsertDesc); ?></p></div>
<?php } ?>
<?php if($errorInsertDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorInsertDesc); ?></p></div>
<?php } ?>

<form action="categorie.html" method="post">
	<p><label>Catégorie</label>
	<select name="insertDescIdCateg">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Titre</label></p>
	<p><textarea name="titre" rows="2" cols="70"></textarea></p>
	<p><label>Description</label></p>
	<p><textarea name="description" rows="20" cols="150"></textarea></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>
<br>


<h3>Mettre à jour une description</h3>

<?php if($successUpdateDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successUpdateDesc); ?></p></div>
<?php } ?>
<?php if($errorUpdateDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorUpdateDesc); ?></p></div>
<?php } ?>

<form action="categorie.html" method="post">
	<p><label>Catégorie</label>
	<select name="updateDescIdCateg">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Titre</label></p>
	<p><textarea name="titre" rows="2" cols="70"></textarea></p>
	<p><label>Description</label></p>
	<p><textarea name="description" rows="20" cols="150"></textarea></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>

<h3>Supprimer une description</h3>

<?php if($successDeleteDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successDeleteDesc); ?></p></div>
<?php } ?>
<?php if($errorDeleteDesc != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorDeleteDesc); ?></p></div>
<?php } ?>

<form action="categorie.html" method="post">
	<p><label>Catégorie</label>
	<select name="deleteDescIdCateg">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><input type="submit" class="submit" value="Valider"></p>
</form>

<?php include('../footer.php'); ?>
