<?php
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	include('./../../../service/Marque_service.php');
	
	$successDeleteNom = null;
	$errorDeleteNom = null;
	
	$pdo = getPdo();
	
	//DELETE MARQUE
	if(isset($_POST['deleteIdNom'])){
		$deleteIdNom = $_POST['deleteIdNom'];
		$deleteIdNom = strReplace($deleteIdNom);
		try{
			deleteMarque($pdo, $deleteIdNom);
			$successDeleteNom = "Suppression de la marque réussite.";
		}catch(Exception $e){
			$errorDeleteNom = $e->getMessage();
		}
	}
	
	$marques = getAllMarques($pdo);
?>
<?php include('../header.php'); ?>
<h2>Supprimer une marque</h2>
<?php if($successDeleteNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successDeleteNom); ?></p></div>
<?php } ?>
<?php if($errorDeleteNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorDeleteNom); ?></p></div>
<?php } ?>
<form action="delete.html" method="post">
	<p><label>Nom</label>
	<select name="deleteIdNom">
		<?php $count = count($marques); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $marques[$i]['id'] . '"'); ?>><?php echo($marques[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><input type="submit" value="Valider"><button class=" admin-back-button" ><a href="marque.html">Précédant</a></button></p>
</form>
<br>
<?php include('../footer.php'); ?>
