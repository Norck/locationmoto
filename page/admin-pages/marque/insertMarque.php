<?php
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	include('./../../../service/Marque_service.php');
	
	$successInsertNom = null;
	$errorInsertNom = null;
	
	if(isset($_POST['insertNom'])){
		$marque = $_POST['insertNom'];
		$marque = strReplace($marque);
		try{
			$pdo = getPdo();
			insertMarque($pdo, $marque);
			$successInsertNom = "Insertion réussite.";
		}catch(PDOException $e){
			$errorInsertNom = $e->getMessage();
		}
	}
	
?>
<?php include('../header.php'); ?>
<h2>Entrez le nom de la marque à insérer </h2>

<?php if($successInsertNom != null){ ?>
		<div class="successBackAdmin"><p><?php echo($successInsertNom); ?></p></div>
	<?php } ?>
	<?php if($errorInsertNom != null){ ?>
		<div class="errorBackAdmin"><p><?php echo($errorInsertNom); ?></p></div>
	<?php } ?>
	
<form action="insert.html" method="post">
	<p><label>Nom</label>
	<input type="text" name="insertNom" placeholder="Mitsubishi"></p>
	<p><input type="submit" value="Valider"><button class=" admin-back-button" ><a href="marque.html">Précédant</a></button></p>
</form>
<br>
<?php include('../footer.php'); ?>
