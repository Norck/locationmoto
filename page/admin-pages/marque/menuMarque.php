<?php 
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	
?>

<?php include('../header.php'); ?>
<div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
					<button class=" admin-back-button" ><a href="../../back.html">Précédant</a></button>
                    <button class=" admin-back-button" ><a href="insert.html">Insérer une nouvelle marque</a></button>
                    <button class=" admin-back-button"><a href="update.html">Mettre à jour une marque</a></button>
                    <button class=" admin-back-button"><a href="delete.html">Supprimer une marque</a></button>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>
