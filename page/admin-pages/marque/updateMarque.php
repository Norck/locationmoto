
<?php
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	include('./../../../service/Marque_service.php');
	
	$successInsertNom = null;
	$errorInsertNom = null;
	
	$successUpdateNom = null;
	$errorUpdateNom = null;
	
	$pdo = getPdo();
	
	//UPDATE NOM
	if(isset($_POST['updateNomMarque']) && isset($_POST['updateIdNom'])){

		$updateNomMarque = $_POST['updateNomMarque'];
		$updateIdNom = $_POST['updateIdNom'];
		
		$updateNomMarque = strReplace($updateNomMarque);
		$updateIdNom = strReplace($updateIdNom);
		
		$success = updateNomMarque($pdo, $updateIdNom, $updateNomMarque);
		if($success == 1){
			$successUpdateNom = "Mise à jour du nom réussit.";
		}else{
			$errorUpdateNom = "Echec de la mise à jour du nom.";
		}
	}
	
	$marques = getAllMarques($pdo);
	
?>
<?php include('../header.php'); ?>
<h2>Mettre à jour une marque</h2>
<?php if($successUpdateNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successUpdateNom); ?></p></div>
<?php } ?>
<?php if($errorUpdateNom != null){ ?>
	<div class="successBackAdmin"><p><?php echo($errorUpdateNom); ?></p></div>
<?php } ?>
<form action="update.html" method="post">
	<p><label>Nom actuel</label>
	<select name="updateIdNom">
		<?php $count = count($marques); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $marques[$i]['id'] . '"'); ?>><?php echo($marques[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Nouveau nom</label>
	<input type="text" name="updateNomMarque" placeholder="Nissan"></p>
	<p><input type="submit" value="Valider"><button class=" admin-back-button" ><a href="marque.html">Précédant</a></button></p>
</form>
<br>
<?php include('../footer.php'); ?>
