<?php

	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/Modele_service.php');
	
	$success = null;
	$error = null;
	
	$pdo = getPdo();
	if(isset($_POST['idModele'])){
		try{
			deleteModele($pdo, $_POST['idModele']);
			$success = 'Suppression réussite.';
		}catch(Exception $e){
			$error = $e->getMessage();
		}
	}
	$modeles = getAllModeles($pdo);
?>
<?php include('../header.php'); ?>
<h2>Suppression modèle</h2>
<?php if($success != null){ ?>
	<div class="successBackAdmin"><p><?php echo($success); ?></p></div>
<?php } ?>
<?php if($error != null){ ?>
	<div class="errorBackAdmin"><p><?php echo($error); ?></p></div>
<?php } ?>
<form action="delete.html" method="post" enctype="multipart/for">
  <p><label>Nom</label>
	<select name="idModele">
		<?php $count = count($modeles); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $modeles[$i]['id'] . '"'); ?>><?php echo($modeles[$i]['nom']); ?></option>
		<?php } ?>
  <input type="submit" value="Valider" name="submit">
  <button class=" admin-back-button" ><a href="modele.html">Précédant</a></button>
</form>
<?php include('../footer.php'); ?>

