<?php

	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/Photo_service.php');
	
	$success = null;
	$error = null;
	
	$pdo = getPdo();
	if(isset($_POST['nomPhoto'])){
		try{
			deletePhoto($pdo, $_POST['nomPhoto']);
			unlink('../../../img/' . $_POST['nomPhoto']);
			$success = 'Suppression réussite.';
		}catch(Exception $e){
			$error = $e->getMessage();
		}
	}
	$photos = getAllPhotos($pdo);
?>
<?php if($success != null){ ?>
	<div class="successBackAdmin"><p><?php echo($success); ?></p></div>
<?php } ?>
<?php if($error != null){ ?>
	<div class="errorBackAdmin"><p><?php echo($error); ?></p></div>
<?php } ?>
<?php include('../header.php'); ?>
<form action="deletePhoto.html" method="post" enctype="multipart/for">
  Suppression photo:
  <p><label>Nom</label>
	<select name="nomPhoto">
		<?php $count = count($photos); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $photos[$i]['nom'] . '"'); ?>><?php echo($photos[$i]['nommodele']) . ' '. $photos[$i]['nom']; ?></option>
		<?php } ?>
  <input type="submit" value="Valider" name="submit">
  <button class=" admin-back-button" ><a href="modele.html">Précédant</a></button>
</form>
<?php include('../footer.php'); ?>
