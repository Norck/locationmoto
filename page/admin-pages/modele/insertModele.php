<?php
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	include('./../../../service/Categorie.php');
	include('./../../../service/Marque_service.php');
	include('./../../../service/Modele_service.php');
	
	$successInsertModele = null;
	$errorInsertModele = null;
	
	$pdo = getPdo();
	
	if( isset($_POST['nom']) && isset($_POST['idCategorie']) && isset($_POST['idMarque']) && isset($_POST['cylindree']) && isset($_POST['description'])){
		
		$nom = $_POST['nom'];
		$idCategorie = $_POST['idCategorie'];
		$idMarque = $_POST['idMarque'];
		$cylindree = $_POST['cylindree'];
		$description = $_POST['description'];
		
		$nom = strReplace($nom);
		$idCategorie = strReplace($idCategorie);
		$idMarque = strReplace($idMarque);
		$description = strReplace($description);
		
		if(is_numeric($cylindree) == true){
			try{
				insertModele($pdo, $nom, $idCategorie, $idMarque, $cylindree, $description);
				$successInsertModele = "Insertion réussite.";
			}catch(Exception $e){
				$errorInsertModele = $e->getMessage();
			}
		}
		
		
	}
	
	$categories = getAllCategories($pdo);
	$marques = getAllMarques($pdo);
	
?>
<?php include('../header.php'); ?>
<h2>Formulaire insertion modèle </h2>

<?php if($successInsertModele != null){ ?>
		<div class="successBackAdmin"><p><?php echo($successInsertModele); ?></p></div>
	<?php } ?>
	<?php if($errorInsertModele != null){ ?>
		<div class="errorBackAdmin"><p><?php echo($errorInsertModele); ?></p></div>
	<?php } ?>
	
<form action="insert.html" method="post">
	<p><label>Nom</label>
	<input type="text" name="nom" placeholder="ZR3"></p>
	<p><label>Catégorie</label>
	<select name="idCategorie">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Marque</label>
	<select name="idMarque">
		<?php $count = count($marques); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $marques[$i]['id'] . '"'); ?>><?php echo($marques[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Cylindrée</label>
	<input type="text" name="cylindree" placeholder="630"></p>
	<p><label>Description</label></p>
	<p><textarea name="description" rows="30" cols="150"></textarea></p>
	<p><input type="submit" value="Valider"><button class=" admin-back-button" ><a href="modele.html">Précédant</a></button></p>
</form>
<br>
<?php include('../footer.php'); ?>
