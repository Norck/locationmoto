<?php

	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/Modele_service.php');
	include('./../../../service/Photo_service.php');

	$target_dir = '../../../img/product-single/';
	//var_dump($_FILES);
	
	$pdo = getPdo();
	
	$error = null;
	$success = null;
	
	if(isset($_FILES["moto"]) && isset($_POST["id"])){
		$target_file = $target_dir . basename($_FILES["moto"]["name"]);
	
	
		$extensions = array();
		$extensions[0] = "jpg";
		$extensions[1] = "jpeg";
		$extensions[2] = "png";
		
		$extension_ok = false;
		
		for($i = 0; $i < count($extensions); $i++){
			if('image/' . $extensions[$i] == $_FILES["moto"]["type"]){
				$extension_ok = true;
				break;
			}
		}
		
		$size_ok = false;
		if($_FILES["moto"]["size"] <= 500000){
			$size_ok = true;
		}
		
		if($extension_ok != true){
			$error = "Extension non autorisée.";
		}
		if($size_ok != true){
			$error = "La taille de l'image est supérieure à 500Ko.";
		}
		if($extension_ok == true && $size_ok == true){
			if (move_uploaded_file($_FILES["moto"]["tmp_name"], $target_file)) {
				try{
					insertPhoto($pdo, $_POST['id'], $_FILES["moto"]["name"]);
					$success = "La photo ". basename( $_FILES["moto"]["name"]). " a été uploadé.";
				}catch(Exception $e){
					if($e->getMessage() != "Cette photo existe déjà dans la base"){
						unlink($target_dir . $_FILES["moto"]["name"]);
					}
					$error = $e->getMessage();
				}
			} else {
				$error = "Echec de l'upload";
			}
		}
	}
	$modeles = getAllModeles($pdo);
?>
<?php include('../header.php'); ?>
<h2>Insertion photo:</h2>
<?php if($success != null){ ?>
	<div class="successBackAdmin"><p><?php echo($success); ?></p></div>
<?php } ?>
<?php if($error != null){ ?>
	<div class="errorBackAdmin"><p><?php echo($error); ?></p></div>
<?php } ?>
<form action="insertPhoto.html" method="post" enctype="multipart/form-data">
  <p><label>Modèle</label>
	<select name="id">
		<?php for($i = 0; $i < count($modeles); $i++){ ?>
			
			<option value=<?php echo('"' . $modeles[$i]['id'] . '"'); ?>><?php echo($modeles[$i]['nom']); ?></option>
		<?php } ?>
  <input type="file" name="moto" id="moto">
  <input type="submit" value="Upload Image" name="submit">
  <button class=" admin-back-button" ><a href="modele.html">Précédant</a></button>
</form>
<?php include('../footer.php'); ?>
