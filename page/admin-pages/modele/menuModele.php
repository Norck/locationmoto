<?php 
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	
?>

<?php include('../header.php'); ?>
<div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
					<button class=" admin-back-button" ><a href="../../back.html">Précédant</a></button>
                    <button class=" admin-back-button" ><a href="insert.html">Insérer un nouveau modèle</a></button>
                    <button class=" admin-back-button"><a href="update.html">Mettre à jour un modèle</a></button>
                    <button class=" admin-back-button"><a href="insertPhoto.html">Insérer des photos</a></button>
                    <button class=" admin-back-button"><a href="deletePhoto.html">Supprimer des photos</a></button>
                    <button class=" admin-back-button"><a href="delete.html">Supprimer un modèle</a></button>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>
