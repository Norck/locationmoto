
<?php
	
	include('./../../../service/controle-admin.php');
	include_once('./../../../service/Connection.php');
	include('./../../../service/tools.php');
	include('./../../../service/Categorie.php');
	include('./../../../service/Marque_service.php');
	include('./../../../service/Modele_service.php');
	
	$successUpdateModele = null;
	$errorUpdateModele = null;
	
	$pdo = getPdo();
	
	if( isset($_POST['id']) && isset($_POST['nouveauNom']) && isset($_POST['idCategorie']) && isset($_POST['idMarque']) && isset($_POST['cylindree']) && isset($_POST['description'])){
		
		$id = $_POST['id'];
		$nouveauNom = $_POST['nouveauNom'];
		$idCategorie = $_POST['idCategorie'];
		$idMarque = $_POST['idMarque'];
		$cylindree = $_POST['cylindree'];
		$description = $_POST['description'];
		
		$id = strReplace($id);
		$nouveauNom = strReplace($nouveauNom);
		$idCategorie = strReplace($idCategorie);
		$idMarque = strReplace($idMarque);
		$description = strReplace($description);
		
		if(is_numeric($cylindree) == true){
			try{
				updateModele($pdo, $id, $nouveauNom, $idCategorie, $idMarque, $cylindree, $description);
				$successUpdateModele = "Mise à jour réussite.";
			}catch(Exception $e){
				$errorUpdateModele = $e->getMessage();
			}
		}
	}
	
	$modeles = getAllModeles($pdo);
	$categories = getAllCategories($pdo);
	$marques = getAllMarques($pdo);
	
?>
<?php include('../header.php'); ?>
<h2>Formulaire mise à jour modèle </h2>

<?php if($successUpdateModele != null){ ?>
	<div class="successBackAdmin"><p><?php echo($successUpdateModele); ?></p></div>
<?php } ?>
<?php if($errorUpdateModele != null){ ?>
	<div class="errorBackAdmin"><p><?php echo($errorUpdateModele); ?></p></div>
<?php } ?>
	
<form action="update.html" method="post">
	<p><label>Nom</label>
	<select name="id">
		<?php $count = count($modeles); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $modeles[$i]['id'] . '"'); ?>><?php echo($modeles[$i]['nom']); ?></option>
		<?php } ?>
	</select><label> Nouveau nom</label>
	<input type="text" name="nouveauNom" placeholder="ZR4"></p>
	<p><label>Catégorie</label>
	<select name="idCategorie">
		<?php $count = count($categories); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $categories[$i]['id'] . '"'); ?>><?php echo($categories[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Marque</label>
	<select name="idMarque">
		<?php $count = count($marques); ?>
		<?php for($i = 0; $i < $count; $i++){ ?>
			<option value=<?php echo('"' . $marques[$i]['id'] . '"'); ?>><?php echo($marques[$i]['nom']); ?></option>
		<?php } ?>
	</select></p>
	<p><label>Cylindrée</label>
	<input type="text" name="cylindree" placeholder="630"></p>
	<p><label>Description</label></p>
	<p><textarea name="description" rows="30" cols="150"></textarea></p>
	<p><input type="submit" value="Valider"><button class=" admin-back-button" ><a href="modele.html">Précédant</a></button></p>
</form>
<br>
<?php include('../footer.php'); ?>
