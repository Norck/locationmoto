<?php
	session_start();

	include('../service/Connection.php');
	include_once('./../modele/Moto.php');
	include_once('./../modele/DescCategMoto.php');
	include('../service/Categorie.php');
	
	$idCategorie = $_GET['idCategorie'];
	$pdo = getPdo();
	$motos = getListeMoto($pdo, $idCategorie);
	$desc = getDescCategorie($pdo, $idCategorie);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content=<?php echo('"' . $desc->getMeta() . '"'); ?>>
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <title><?php echo($desc->getPageTitle()); ?></title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/minified.css" type="text/css">
    <script src="js/minified.js" defer></script>
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

	<!-- Header Section Begin -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        locationsoa@gmail.com
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        032 45 654 67	| 034 46 574 58
                    </div>
                </div>
                <div class="ht-right">
						<?php if(isset($_SESSION['nomUtilisateur'])){ ?>
								<a href="logout.php" class="login-panel"><i class="fa fa-user"></i><?php echo($_SESSION['nomUtilisateur'] . ''); ?> Se déconnecter</a>
						<?php }else{ ?>
							<a href="login.html" class="login-panel"><i class="fa fa-user"></i>Se connecter</a>
						<?php } ?>
						
					
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner-header">
				<div class="row">
                    <div class="">
                        <div class="logo">
                            <h1 style="color:#FFCF64;font-size:25px;">Location moto Soa Analakely</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="advanced-search">
                            <button type="button" class="category-btn">All Categories</button>
                            <div class="input-group">
                                <input type="text" placeholder="What do you need?">
                                <button type="button"><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 text-right col-md-3">
                        <ul class="nav-right">
                            <li class="heart-icon">
								
                            </li>
                            <li class="cart-icon">
                                
                            </li>
                            <li class="cart-price"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <li class="active"><a href="./index.html"><h4>Home</h4></a></li>
                        <li><a href="#"><h4>Catégorie</h4></a>
                            <ul class="dropdown">
                                <li><a href="categorie-categM2.html">Roadster</a></li>
                                <li><a href="categorie-categM1.html">Routière</a></li>
                                <li><a href="categorie-categM3.html">Tout-terrain</a></li>
                            </ul>
                        </li>
                        <li><a href="./promotion.html"><h4>Promotions</h4></a></li>
                        <li><a href="./contact.html"><h4>Contact</h4></a></li>
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
    <!-- Header End -->



    <!-- Banner Section Begin -->
    <div class="banner-section spad">
        <div class="container-fluid">
			
			<h1><?php echo('<h1>' . $desc->getTitre() . '</h1>'); ?></h1>
			<?php echo($desc->getDescription()); ?>
			
			<h2>Nos motos <?php echo($motos[0]->getCategorie()); ?></h2>
            <div class="row">
				<?php $count = count($motos); ?>
				<?php for($i = 0; $i < $count; $i++){ ?>
					<div class="col-lg-4">
						<div class="single-banner">
							<img src=<?php echo('"img/product-single/' . $motos[$i]->getPhotos() . '"'); ?> alt="moto-roadster-suzuki-gsx-s1000">
							<div class="inner-text">
								<a href=<?php echo('"./produit-' . $motos[$i]->getIdModele() . '.html"'); ?>><h3 style="font-size:15px;"><?php echo($motos[$i]->getMarque()); ?> <?php echo($motos[$i]->getModele()); ?></h3></a>
							</div>
						</div>
					</div>
				<?php } ?>
			
            </div>
        </div>
    </div>
    <!-- Banner Section End -->

	
    <!-- Partner Logo Section Begin -->
    <div class="partner-logo">
        <div class="container">
            <div class="logo-carousel owl-carousel">
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/bni-madagascar.jpg" alt="bni madagascar">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/airtel-money.jpg" alt="airtel money">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="img/logo-carousel/m-vola.jpg" alt="m vola">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partner Logo Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->
</body>

</html>


