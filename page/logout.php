<?php
	session_start();
	
	include('./../service/Connection.php');
	include('./../service/Token_service.php');
	
	if(isset($_SESSION['idToken'])){
		$pdo = getPdo();
		unvalidateToken($pdo, $_SESSION['idToken']);
		session_destroy();
	}
	header('Location: index.html');
?>
