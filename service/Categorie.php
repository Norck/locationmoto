<?php
	
	function getPhoto($pdo, $motos){
		$count = count($motos);
		try{
			for($i = 0; $i < $count; $i++){
				$query = 'SELECT * FROM photo where idmodele = \'' . $motos[$i]->getIdModele() . '\' limit 1';
				$st = $pdo->prepare($query);
				$st->execute();

				$moto = array();
				$row = $st->fetch(PDO::FETCH_ASSOC);
				$motos[$i]->setPhotos($row['nom']);
			}
			return $motos;
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
		
		
	}
	
	
	function getListeMoto($pdo, $idCategorie){
		$st = null;
		try{
			$query = 'SELECT * FROM getMotoFromCategorie where idCategorie = \'' . $idCategorie . '\'';
			$st = $pdo->prepare($query);
			$st->execute();

			$moto = array();
			$i = 0;
			while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
				
				$id = '';
				$idModele = $row['idmodele'];
				$modele = $row['nommodele'];
				$idMarque = $row['idmarque'];
				$marque = $row['nommarque'];
				$tarifs = '';
				$description = '';
				$idCategorie = $row['idcategorie'];
				$categorie = $row['nomcategorie'];
				$cylindree = '';
				$photos = '';
				
				$moto[$i] = new Moto($id, $idModele, $modele, $idMarque, $marque, $tarifs, $description, $idCategorie, $categorie, $cylindree, $photos);
				
				$i++;
			}
			
			$moto = getPhoto($pdo, $moto);
			
			return $moto;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
		
	}
	
	function getDescCategorie($pdo, $idCategorie){
		$query = '';
		try{
			$query = 'select * from descCategMoto where idCategorie = \'' . $idCategorie . '\'';
			$st = $pdo->prepare($query);
			$st->execute();

			$row = $st->fetch(PDO::FETCH_ASSOC);
			
			$desc = new DescCategMoto($row['idcategorie'], $row['titre'], $row['description'], $row['pagetitle'], $row['meta']);
			
			return $desc;
			
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			var_dump($query);
		}
	}
	
	function insererNomCategorie($pdo, $nom){
		try{
			$query = 'select * from categorieMoto where upper(nom) = upper(\'' . $nom . '\')';
			$st = $pdo->prepare($query);
			$st->execute();

			$row = $st->fetch(PDO::FETCH_ASSOC);
			if($row != null){
				return 0;
			}
			$query = 'insert into categorieMoto values(\'categM\' || nextval(\'idCategorieMoto\'), \'' . $nom . '\')';
			$st = $pdo->prepare($query);
			$st->execute();
			$count = $st->rowCount();
			if($count == 1){
				return 1;
			}else{
				return 0;
			}
			
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			return 0;
		}
		
	}
	
	function getAllCategories($pdo){
		try{
			$query = 'select * from categorieMoto';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$categories = array();
			$i = 0;
			while($row = $st->fetch(PDO::FETCH_ASSOC)){
				$categories[$i]['id'] = $row['id'];
				$categories[$i]['nom'] = $row['nom'];
				$i++;
			}
			
			return $categories;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			$categories = array();
			return $categories;
		}
	}
	
	function updateNomCategorie($pdo, $idNom, $nouveauNom){
		try{
			$query = 'update categorieMoto set nom=\'' . $nouveauNom . '\' where id=\''. $idNom . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			return 0;
		}
	}
	
	function insertDescription($pdo, $idCategorie, $titre, $description){
		try{
			$query = 'insert into descCategMoto values(\'' . $idCategorie . '\', \'' . $titre . '\', \'' . $description . '\')';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				return 0;
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			
			return 0;
		}
	}
	
	function updateDescription($pdo, $idCategorie, $titre, $description){
		try{
			$query = 'update descCategMoto set titre=\'' . $titre . '\' and description=\'' . $description . '\' where idCategorie=\'' . $idCategorie . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				return 0;
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			
			return 0;
		}
	}
	
	function deleteDescription($pdo, $idCategorie){
		try{
			$query = 'delete from descCategMoto where idCategorie = \'' . $idCategorie . '\'';
			$count = $pdo->exec($query);
			if($count == 1){
				return 1;
			}else{
				return 0;
			}
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			
			return 0;
		}
	}
	
	function deleteCategorie($pdo, $idCategorie){
		try{
			$pdo->beginTransaction();
			$query = 'delete from descCategMoto where idCategorie = \'' . $idCategorie . '\'';
			$count = $pdo->exec($query);
			$query = 'delete from categorieMoto where id=\'' . $idCategorie . '\'';
			$count = $pdo->exec($query);
			if($count == 0){
				$pdo->rollBack();
				throw new Exception('Impossble de supprimer cette catégorie, une ou plusieurs modèles existantes dependent d\'elle');
			}
			$pdo->commit();
			
		}catch(PDOException $e){
			$pdo->rollBack();
			throw $e;
		}
	}
	
	
?>
