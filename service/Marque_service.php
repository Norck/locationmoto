<?php
	function insertMarque($pdo, $marque){
		try{
			$query = 'insert into marqueMoto values(\'marque\' || nextval(\'idMarqueMoto\'), \'' . $marque . '\')';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				return 0;
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			
			return 0;
		}
	}
	function updateNomMarque($pdo, $updateIdNom, $updateNomMarque){
		try{
			$query = 'update marqueMoto set nom=\'' . $updateNomMarque . '\' where id=\'' . $updateIdNom . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				return 0;
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			
			return 0;
		}
	}
	
	function getAllMarques($pdo){
		try{
			$query = 'select * from marqueMoto';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$marques = array();
			$i = 0;
			while($row = $st->fetch(PDO::FETCH_ASSOC)){
				$marques[$i]['id'] = $row['id'];
				$marques[$i]['nom'] = $row['nom'];
				$i++;
			}
			
			return $marques;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			$marques = array();
			return $marques;
		}
	}
	
	function deleteMarque($pdo, $idMarque){
		try{
			$query = 'delete from marqueMoto where id = \'' . $idMarque . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				throw new Exception('Echec de la suppression.');
			}
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			throw new Exception('Echec de la suppression.');
		}
	}
?>
