<?php
	function modeleAlreadyExist($pdo, $nom){
		try{
			$query = 'select * from modele where nom = \'' . $nom . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() != 0){
				return true;
			}
			return false;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			return true;
		}
	}

	function insertModele($pdo, $nom, $idCategorie, $idMarque, $cylindree, $description){
		try{
			if(modeleAlreadyExist($pdo, $nom) == true){
				throw new Exception('Ce modèle existe déjà.');
			}
			$query = 'insert into modele values(\'modele\' || nextval(\'idModele\'), \'' . $nom . '\', \'' . $idCategorie . '\', \'' . $idMarque . '\', ' . $cylindree . ', \'' . $description . '\')';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				throw new Exception('Insertion échouée.');
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			throw new Exception('Insertion échouée.');
		}
	}
	
	function updateModele($pdo, $id, $nouveauNom, $idCategorie, $idMarque, $cylindree, $description){
		try{
			$query='update modele set nom=\'' . $nouveauNom . '\' , idCategorie=\'' . $idCategorie . '\' , idMarque=\'' . $idMarque . '\' , moteur=' . $cylindree . ' , description=\'' . $description . '\' where id=\'' . $id . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				throw new Exception('Mise à jour du modèle échouée.');
			}
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			throw new Exception('Mise à jour du modèle échouée.');
		}
	}
	
	function getAllModeles($pdo){
		try{
			$query = 'select id, nom from modele';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$categories = array();
			$i = 0;
			while($row = $st->fetch(PDO::FETCH_ASSOC)){
				$categories[$i]['id'] = $row['id'];
				$categories[$i]['nom'] = $row['nom'];
				$i++;
			}
			
			return $categories;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			$categories = array();
			return $categories;
		}
	}
	
	function deleteModele($pdo, $idModele){
		$pdo->beginTransaction();
		$imageNotFound = array();
		$i = 0;
		try{
			$query = 'select * from photo where idModele = \'' . $idModele . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			while($row = $st->fetch(PDO::FETCH_ASSOC)){
				$query = 'delete from Photo where id=\'' . $row['id'] . '\'';
				$pdo->exec($query);
				try{
					unlink('../../../img/product-single' . $row['nom']);
				}catch(Exception $e){
					$imageNotFound[$i] = $row['nom'];
					$i++;
				}
			}
			$query = 'delete from modele where id=\'' . $idModele . '\'';
			$count = $pdo->exec($query);
			if($count == 0){
				$pdo->rollBack();
				throw new Exception('Echec de la suppression, modèle non trouvée.');
			}
			$pdo->commit();
		}catch(PDOException $e){
			$pdo->rollBack();
			throw($e);
		}
	}
?>
