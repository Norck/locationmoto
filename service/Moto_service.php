<?php
	
	include_once('../modele/Moto.php');
	include_once('../service/Tarif_service.php');
	
	function getPhotos($pdo, $idModele){
		
		try{
			
			$query = 'select nom from photo where idModele=\'' . $idModele . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$photos = array();
			$i = 0;
			
			while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
				$photos[$i] = $row['nom'];
				$i++;
			}
			
			return $photos;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
	}

	function getMoto($pdo, $idModele){
		try{
			$query = 'select * from ficheMoto where idModele=\'' . $idModele . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$row = $st->fetch(PDO::FETCH_ASSOC);
			
			$id = '';
			$idModele = $row['idmodele'];
			$modele = $row['nommodele'];
			$idMarque = $row['idmarque'];
			$marque = $row['nommarque'];
			$tarifs = '';
			$description = $row['description'];
			$idCategorie = $row['idcategorie'];
			$categorie = $row['nomcategorie'];
			$cylindree = $row['cylindree'];
			$photos = '';
			
			$moto = new Moto($id, $idModele, $modele, $idMarque, $marque, $tarifs, $description, $idCategorie, $categorie, $cylindree, $photos);
			$tarifs = getTarifs($pdo, $idModele);
			$moto->setTarifs($tarifs);
			$photos = getPhotos($pdo, $idModele);
			$moto->setPhotos($photos);
			return $moto;
			
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
	}

?>
