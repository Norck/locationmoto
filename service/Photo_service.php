<?php
	function photoAlreadyExist($pdo, $photoName){
		try{
			$query = 'select * from photo where nom = \'' . $photoName . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			$row = $st->fetch(PDO::FETCH_ASSOC);
			if($row != null){
				return true;
			}
			return false;
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
		
	}

	function insertPhoto($pdo, $idModele, $photoName){
		if(photoAlreadyExist($pdo, $photoName)){
			throw new Exception("Cette photo existe déjà dans la base");
		}
		try{
			$query = 'insert into Photo values(\'pic\' || nextval(\'idPhoto\'), \'' . $photoName . '\', \'' . $idModele . '\')';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				throw new Exception('Insertion échouée.');
			}
			return 1;
		} catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			throw new Exception('Insertion échouée.');
		}
	}
	
	function deletePhoto($pdo, $nom){
		try{
			$query = 'delete from Photo where nom=\'' . $nom . '\'';
			$st = $pdo->prepare($query);
			$st->execute();
			if($st->rowCount() == 0){
				throw new Exception("Photo non existante");
			}
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
			throw new Exception("Echec de la suppression de la photo.");
		}
	}
	
	function getAllPhotos($pdo){
		$photos = array();
		try{
			$query = 'select * from infoPhoto';
			$st = $pdo->prepare($query);
			$st->execute();
			
			$i = 0;
			
			while($row = $st->fetch(PDO::FETCH_ASSOC)){
				$photos[$i] = $row;
				$i++;
			}
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
		return $photos;
	}
?>
