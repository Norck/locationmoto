<?php

	function getToken($pdo, $idUtilisateur){
		try{
			
			$query = 'select token.*, utilisateur.nom from token join utilisateur on token.idUtilisateur = utilisateur.id where idUtilisateur = \'' . $idUtilisateur . '\' and etat = 1';
			//printAsP($query);
			$st = $pdo->prepare($query);
			$st->execute();
			
			$row = $st->fetch(PDO::FETCH_ASSOC);
			if($row != null){
				return $row;
			}
			
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
	}

	function generateToken($pdo, $idUtilisateur, $minuteValidite){
		
		date_default_timezone_set("UTC");
		$creation = date("Y-m-d H:i:s" ,time());
		$validiteSec = time() + $minuteValidite * 60;
		$expiration = date("Y-m-d H:i:s" , $validiteSec);
		
		try{
			$query = 'insert into token values(\'idToken\' || nextval(\'idToken\'), \'' . $idUtilisateur . '\', \'' . $creation . '\', \'' . $expiration . '\', 1)';
			//printAsP($query);
			$st = $pdo->prepare($query);
			$st->execute();
			
			$token = getToken($pdo, $idUtilisateur);
			if($token != null){
				$_SESSION['nomUtilisateur'] = $token['nom'];
				$_SESSION['idToken'] = $token['id'];
				return $token['id'];
			}else{
				
			}
			
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
		
	}
	
	function unvalidateToken($pdo, $idToken){
		try{
			$query = 'update token set etat=1 where id=\'' . $idToken . '\';';
			//printAsP($query);
			$st = $pdo->prepare($query);
			$st->execute();
			
		}catch(PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		}
	}
?>
