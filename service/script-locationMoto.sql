sudo -u postgres psql postgres
drop database locationmoto;
create database locationmoto;
\c locationmoto;

drop sequence idUtilisateur;
create sequence idUtilisateur;

drop table utilisateur cascade;
create table utilisateur(
	id varchar(10) primary key not null,
	nom varchar(10) not null,
	email varchar(50) not null,
	mdp varchar(50) not null
); 

drop table admin cascade;
create table admin(
	idUtilisateur varchar(10) references utilisateur(id) primary key
);

drop sequence idToken;
create sequence idToken;

drop table token cascade;
create table token(
	id varchar(50) primary key not null,
	idUtilisateur varchar(10) not null,
	creation timestamp not null,
	expiration timestamp not null,
	etat smallint
);


drop sequence idVendeur;
create sequence idVendeur;

drop table vendeur cascade;
create table vendeur(
	id varchar(10) primary key not null, 
	nom varchar(10) not null, 
	tel varchar(13) not null, 
	embauche date not null
);

drop sequence idCategorieMoto;
create sequence idCategorieMoto;

drop table categorieMoto cascade;
create table categorieMoto(
	id varchar(10) primary key not null,
	nom varchar(30) not null
);

drop sequence marqueMoto;
create sequence idMarqueMoto;

drop table marqueMoto cascade;
create table marqueMoto(
	id varchar(10)  primary key  not null,
	nom varchar(30) not null
);	

drop sequence idModele;
create sequence idModele;

drop table modele cascade;
create table modele(
	id varchar(10) primary key not null,
	nom varchar(30) not null,
	idCategorie varchar(10) references categorieMoto(id),
	idMarque varchar(10) references marqueMoto(id),
	moteur smallint not null,
	description text not null
);

drop sequence idPhoto;
create sequence idPhoto;

drop table photo cascade;
create table photo(
	id varchar(10) not null primary key,
	nom varchar(150) not null,
	idModele varchar(10) references modele(id)
);

drop view infoPhoto cascade;
create view infoPhoto as select photo.id, photo.nom, modele.nom as nomModele from photo join modele on photo.idModele = modele.id;

drop sequence idTarif;
create sequence idTarif;

drop table tarif cascade;
create table tarif(
	id varchar(10) not null primary key,
	idModele varchar(10) references modele(id),
	nbJour smallint not null,
	prix float not null,
	caution float not null
);

drop table descCategMoto cascade;
create table descCategMoto(
	idCategorie varchar(10) references categorieMoto(id) primary key,
	titre varchar(100) not null,
	description text not null,
	pageTitle varchar(200) not null,
	meta text not null
);

insert into descCategMoto values('categM2', 'La moto roadster', '<h2>Un roadster, une machine pour ceux avides de puissance</h2><p>Un <strong>roadster</strong> est par définition un moto de sport priorisant la puissance au détriment du confort . Mais cela lui permet d''avoir l''allure d''une machine fait pour la course et attire l''attention des passants surtout avec les bruits qu''il engendre , une parfaite machine pour la frime .</p>
<p>Aussi un <strong>motard</strong> aime bien exposer son jouet lors des conventions et randonnées en empruntant les routes nationales où il peut profiter de la <strong>puissance</strong> de sa machine ainsi sentir l''adrenaline monter en lui et jouir de la vitesse.C''est donc un engin compatible pour la vie urbaine et longs voyages.</p>
', 'La moto roadster-Catégorie-Location moto Soa Analakely');
insert into descCategMoto values('categM1', 'Une moto routière','<h2>La moto routière, le confort sur 2 roues</h2><p>La  <strong>moto</strong> <strong>routière</strong> est la plus évidente pour des longs trajets. Elle se distingue par son imposante masse ce qui la rend difficile à manœuvrer dans un espace restreint comme passer entre 2 voitures dans un embouteillage. Et logiquement elle est une <strong>grosse cylindrée</strong> afin de tracter son énorme poids.</p>
<p>Au début elle peut sembler repoussante pour les débutants, mais elle est très <strong>confortable</strong> du fait de ses nombres options. En effet, ses poignées sont chauffantes ainsi que la selle, la suspension est facilement ajustable, sa bulle électrique presque isole le motard de la rafale de vent devant lui, mais aussi elle protège mieux les jambes du conducteur. Et peut- être vous êtes  attiré par son importante capacité de <strong>bagagerie</strong>. </p>
<p>Et parce qu'' une moto routière coûte chère, et que pas tout le monde l''utilise tous les jours nous vous offrons la possibilité de louer une pour une moindre somme pendant quelque jours.Et si jamais vous pensez que c''est trop overkill, vous pouvez toujours opter pour le <a href="categorie-categM2.html"><strong>roadster</strong></a>.</p>');

insert into descCategMoto values('categM3', 'La moto tout terrain', '<h2> La moto tout terrain, le couteau suisse des 2 roues</h2><p>Avez-vous songer à vous rendre à un lieu lointain mais que le chemin à prendre est tellement long et impossible à prendre avec une voiture où à pied ?Est-ce que vous aimez tout simplement vous amuser avec une 2 roues sur une terre no revêtue ? La solution est sans doute une <strong>moto tout terrain<strong> capable de surmonter presque tous les mauvais traitements possibles.</p>
<p>En général, une <strong>moto tout terrain</strong> a une forme effilée, une selle et guidon en hauteur pour plus de visibilité des obstacles et de l''équilibre. Son moteur peu puissant est fait pour le parcours d''obstacle et non pour la grande vitesse. Que dire, adorés des personnes qui aiment le <strong>sport extrême</strong> sur 2 roues.</p>
<p>Il convient à vous de choisir entre trial, enduro, cross et super motard selon le type de besoin que vous avez. Ce dernier étant populaire et relativement <strong>pas chère</strong> du tout, ainsi est aussi le prix de <strong>location</strong> que nous offrons chez <strong>Location moto Soa</strong>.<p>');

--data--


insert into utilisateur values('user' || nextval('idUtilisateur'), 'Ravaka', 'ravaka@gmail.com', 'ravaka');
insert into utilisateur values('user' || nextval('idUtilisateur'), 'Niaina', 'niaina@gmail.com', 'niaina');

insert into admin values('user2');

insert into vendeur values('vdr' || nextval('idVendeur'), 'ravaka', '034 50 600 58', '2020-08-20');
insert into vendeur values('vdr' || nextval('idVendeur'), 'niaina', '033 40 323 92', '2020-09-20');
insert into vendeur values('vdr' || nextval('idVendeur'), 'Koto', '032 23 320 54', '2020-10-20');

insert into categorieMoto values('categM' || nextval('idCategorieMoto'), 'routière');
insert into categorieMoto values('categM' || nextval('idCategorieMoto'), 'roadster');
insert into categorieMoto values('categM' || nextval('idCategorieMoto'), 'tout-terrain');

insert into marqueMoto values('marque' || nextval('idMarqueMoto'), 'Suzuki');
insert into marqueMoto values('marque' || nextval('idMarqueMoto'), 'Honda');
insert into marqueMoto values('marque' || nextval('idMarqueMoto'), 'Yamaha');
insert into marqueMoto values('marque' || nextval('idMarqueMoto'), 'Harley Davidson');
insert into marqueMoto values('marque' || nextval('idMarqueMoto'), 'BMW');



insert into modele values('modele' || nextval('idModele'), 'GSX-S1000', 'categM2', 'marque1', 999, 'La GSX-S1000 est propulsée par une version street-tuned du moteur quatre temps à DACT refroidi par liquide de 999 cm3, qui est devenu une légende dans la GSX-R1000 2005-2008. Le moteur aiguisé offre une réponse douce à l''accélérateur et une accélération immédiate et contrôlée, de sorte que le pilote sportif éprouve des performances à l''adrénaline.

Une conception à longue course avec un alésage de 73,4 mm et une course de 59,0 mm permet aux chambres de combustion d''être compactes. Il permet donc de combiner un taux de compression optimal, une forme de piston à sommet plat et une large répartition de la puissance sur toute la plage de régime.

Les avances à l''intérieur du moteur commencent par les pistons. Suzuki a utilisé des techniques d''analyse par éléments finis pour rendre les pistons légers sans compromettre leur rigidité. Les avantages comprennent un couple élevé et une accélération rapide.

Les profils de came optimisent le calage des soupapes pour obtenir des caractéristiques de puissance parfaitement adaptées aux rues de la ville et aux routes de banlieue sinueuses. Les bougies d''allumage Iridium assurent de fortes étincelles pour une combustion efficace qui se traduit par une puissance plus élevée, une réponse linéaire de l''accélérateur, un démarrage plus facile du moteur et un ralenti stable.

Chaque alésage est plaqué avec le revêtement nickel-phosphore-silicium-carbure éprouvé de Suzuki, qui réduit la friction, améliore le transfert de chaleur, la durabilité et le joint annulaire, et est connu sous le nom de Suzuki Composite Electrochemical Material (SCEM).

Le radiateur avec une forme ronde efficace aide à maintenir la température du moteur constante. Les enveloppes guident l''air vers le noyau du radiateur. Un refroidisseur d''huile refroidi par liquide est léger et compact. Ses petites dimensions contribuent à donner à la moto un aspect soigné en créant plus de place pour le système d''échappement.');



insert into modele values('modele' || nextval('idModele'), 'XL1200CX', 'categM2', 'marque4', 1202, 'Un plus grand ensemble de jantes en alliage léger de 19 pouces à l''avant et de 18 pouces à l''arrière, ainsi que des amortisseurs arrière plus longs et de grosses fourches USD de 43 mm à l''avant modifient l''orientation complète du cadre de berceau en acier tubulaire familier du 1200, le rendant nettement plus `` cul en haut, piqué en bas '', dans le plus pur style café racer.

Et cela, associé à une configuration de freins à double disque décente complète avec des étriers à quatre pistons doubles, un guidon semi-`` ace '''', plus de repose-pieds arrière et un siège de course magnifiquement conçu s''ajoute à un vélo qui est à la fois un jeu d''enfant. continuer et celui qui vous pousse à vous bousculer et à gratter.

C''est un plaisir de diriger, ce qui est rapide et précis, la conduite est plus contrôlée et sophistiquée que n''importe quel Sportster précédent et les freins sont si bons que c''est comme si Harley avait enfin découvert à quel point les freins modernes peuvent être bons.


Le bicylindre en V familier de 1202 cm3, refroidi par air, est totalement inchangé par rapport à celui de ses frères et sœurs croiseurs.

Augmenter la capacité de 30% à 1202 cm3 a eu un effet évident sur la puissance, de sorte que la masse frémissante de Milwaukee produit maintenant 66 chevaux et 73 pieds-livres de couple - une augmentation significative par rapport au 883.

Cela lui donne beaucoup plus de conduite en sortie de virage et lorsque vous vous amusez à conduire, vous pouvez simplement le laisser en troisième vitesse et rouler partout sur une vague de couple. Pour être honnête, c''est la façon la plus agréable de rouler.

Si vous voulez en tirer le maximum de puissance, vous pouvez le faire tourner jusqu''à ce que les quatre soupapes commencent à danser sur le dessus de la culasse, mais tout ce que vous ferez est de vous donner mal aux oreilles et de brûler tout votre carburant sans faire grand chose. plus de progrès. Au lieu de cela, asseyez-vous, détendez-vous et faites du bruit.');



insert into modele values('modele' || nextval('idModele'), 'DL650', 'categM1', 'marque1', 645, 'La Suzuki V-Strom 650 (DL650) est une moto de tourisme de poids moyen lancée en 2004 avec une posture de conduite standard, une injection de carburant et un châssis en aluminium - maintenant dans sa troisième génération depuis l''année modèle 2017. Commercialisé en Europe, en Océanie, dans les Amériques et depuis 2018 en Inde, le DL650 est fabriqué dans l''usine d''assemblage final certifiée ISO 14001 de Suzuki à Toyokawa, au Japon.

Le V-Strom 650 commercialise la force dans une seule zone pour l''adaptabilité à une variété de conditions de conduite: navettage, croisière, tourisme d''aventure et à un degré moindre, conduite hors route. Le vélo est diversement catégorisé comme double sport, sport enduro tourer, [7] rue / aventure, banlieue ou entrée de gamme.

Selon le New York Times, le V-Strom a un public fidèle dans le monde entier, et le DL650 dépasse les ventes du plus grand DL1000 de Suzuki deux à un.

Le nom V-Strom combine V, se référant à la configuration du moteur V de la moto, avec le mot allemand strom, qui signifie flux ou courant.');


insert into modele values('modele' || nextval('idModele'), 'K 1600 GT', 'categM1', 'marque5', 1649, 'La K 1600 GTL se veut moto de tous les superlatifs et des toutes dernières technologies, privilégiant une orientation tourisme et duo. Sa motorisation six cylindres très compacte permet de contenir les volumes en largeur, et d''offrir un agrément très rare dans la production actuelle, fait de force et d''onctuosité. Doté d''origine d''assistances, le bloc propose 3 modes d''injection (pluie, route et dynamique), ainsi qu''un contrôle de traction d''autant plus utile qu''il lui faudra gérer 70 % du couple disponible dès 1500 tr/min... On retrouve également en option un d''éclairage gardant toujours l''horizon, une grande première sur une moto de série. La hauteur de selle étant réglable de 810 à 830 mm. Côté dynamique, la K1600 est une moto des plus surprenante, faisant totalement oublier son poids. Le cadre coquille en aluminium permet d''offrir un comportement d''autant plus sain que les redoutables suspensions Duolever et Paralever officient avec brio. L''amortissement pourra bien entendu être piloté par l''ESA depuis des commodos d''une complexité déroutante tant ils sont complets. Les pouces ont fort à faire pour s''y retrouver !');


insert into modele values('modele' || nextval('idModele'), 'RM-Z450', 'categM3', 'marque1', 449, 'Le modèle Suzuki RMZ 450 est un vélo Cross / Motocross fabriqué par Suzuki. Dans cette version vendue à partir de l''année 2010, le poids à sec est et il est équipé d''un moteur monocylindre à quatre temps. Le moteur produit une puissance de sortie de crête maximale de et un couple maximal de. Avec ce groupe motopropulseur, la Suzuki RMZ 450 est capable d''atteindre une vitesse de pointe maximale de. En ce qui concerne les caractéristiques du châssis, responsable de la tenue de route, du comportement de conduite et du confort de conduite, les Suzuki RMZ 450 ont un cadre avec suspension avant SHOWA 47mm télescopique, pneumatique / ressort hélicoïdal, amorti à l''huile et dans la suspension arrière il est équipé de type bras oscillant. , Amortisseur SHOWA ferroutage. Les tailles de pneus de série sont 80 / 100-21 à l''avant et 110 / 90-19 à l''arrière. En ce qui concerne la puissance de freinage, le système de freinage Suzuki RMZ 450 comprend une taille de disque unique à l''avant et une taille de disque unique à l''arrière.');


insert into modele values('modele' || nextval('idModele'), 'YZ250F', 'categM3', 'marque3', 250, 'Il n''est pas inhabituel pour une motocyclette d''obtenir un total complet un an et très peu de changements, voire aucun, l''année suivante. La Yamaha YZ250F 2020 en est un excellent exemple. En 2019, la YZ250F a fait l''objet d''une mise à jour importante de son moteur, de son châssis et de sa suspension. Presque aucune pièce n''a été laissée intacte, il n''est donc pas surprenant que la YZ250F 2020 arrive presque identique à son prédécesseur. Mais cela ne veut pas dire que nous ne voulons pas le conduire! Alors, quand nous avons eu la chance de passer une jambe sur la YZ250F 2020, nous l''avons prise et nous nous sommes dirigés vers la piste pour être acquittés à nouveau avec la nouvelle YZ que, eh bien, nous savions déjà.

Il n''''est pas inhabituel pour une motocyclette d''''obtenir un total complet un an et très peu de changements, voire aucun, l''''année suivante. La Yamaha YZ250F 2020 en est un excellent exemple. En 2019, la YZ250F a fait l''''objet d''''une mise à jour importante de son moteur, de son châssis et de sa suspension. Presque aucune pièce n''''a été laissée intacte, il n''''est donc pas surprenant que la YZ250F 2020 arrive presque identique à son prédécesseur. Mais cela ne veut pas dire que nous ne voulons pas le conduire! Alors, quand nous avons eu la chance de passer une jambe sur la YZ250F 2020, nous l''''avons prise et nous nous sommes dirigés vers la piste pour être acquittés à nouveau avec la nouvelle YZ que, eh bien, nous savions déjà.

Déjà connue pour son moteur puissant, la YZ250F reste fidèle à sa disposition des orifices d''admission et d''échappement symétriques inversés, qui a été introduite au cours de l''année modèle 2014. Cette conception unique permet un courant descendant droit pour l''admission avant et un orifice arrière à sortie centrale , ce qui se traduit par une meilleure efficacité d''écoulement et finalement plus de puissance. Il existe également un piston de haute technologie qui contribue à la large et sympathique bande de puissance de la YZ. Un moteur robuste, combiné à un châssis léger, rend difficile le maintien de la roue avant au sol. Et nous sommes d''accord avec ça.');



insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-suzuki-gsx-s1000-1.jpg', 'modele1');
insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-suzuki-gsx-s1000-2.jpg', 'modele1');
insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-suzuki-gsx-s1000-3.jpg', 'modele1');

insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-harley-davidson-XL1200CX-1.jpg', 'modele2');
insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-harley-davidson-XL1200CX-2.jpg', 'modele2');
insert into photo values('pic' || nextval('idPhoto'), 'moto-roadster-harley-davidson-XL1200CX-3.jpg', 'modele2');

insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-bmw-K-1600-GT-1.jpg', 'modele4');
insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-bmw-K-1600-GT-2.jpg', 'modele4');
insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-bmw-K-1600-GT-3.jpg', 'modele4');

insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-suzuki-V-Strom-650-1.jpg', 'modele3');
insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-suzuki-V-Strom-650-2.jpg', 'modele3');
insert into photo values('pic' || nextval('idPhoto'), 'moto-routiere-suzuki-V-Strom-650-3.jpg', 'modele3');

insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-suzuki-RM-Z450-1.jpg', 'modele5');
insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-suzuki-RM-Z450-2.jpg', 'modele5');
insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-suzuki-RM-Z450-3.jpg', 'modele5');

insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-yamaha-YZ250F-1.jpg', 'modele6');
insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-yamaha-YZ250F-2.jpg', 'modele6');
insert into photo values('pic' || nextval('idPhoto'), 'moto-tout-terrain-yamaha-YZ250F-3.jpg', 'modele6');


insert into tarif values('tarif' || nextval('idTarif'), 'modele1', 3, 150000, 100000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele2', 3, 150000, 150000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele3', 3, 200000, 175000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele4', 3, 250000, 200000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele5', 3, 225000, 200000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele6', 3, 100000, 100000);

insert into tarif values('tarif' || nextval('idTarif'), 'modele1', 15, 1300000, 1100000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele2', 15, 2000000, 1750000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele3', 15, 2400000, 2100000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele4', 15, 3400000, 3100000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele5', 15, 3100000, 2800000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele6', 15, 1500000, 1500000);

insert into tarif values('tarif' || nextval('idTarif'), 'modele1', 30, 2300000, 2100000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele2', 30, 3800000, 3500000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele3', 30, 4500000, 4000000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele4', 30, 6400000, 6000000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele5', 30, 5800000, 5500000);
insert into tarif values('tarif' || nextval('idTarif'), 'modele6', 30, 3200000, 3000000);

drop view getMotoFromCategorie cascade;
create view getMotoFromCategorie
as select modele.id as idModele, modele.nom as nomModele, 
modele.idCategorie, categorieMoto.nom as nomCategorie, 
marqueMoto.id as idMarque , marqueMoto.nom as nomMarque
from categorieMoto
join modele
on categorieMoto.id = modele.idCategorie
join marqueMoto
on modele.idMarque = marqueMoto.id;


drop view ficheMoto cascade;
create view ficheMoto
as select modele.id as idModele, modele.nom as nomModele, 
modele.idCategorie, modele.description, modele.moteur as cylindree,
categorieMoto.nom as nomCategorie, 
marqueMoto.id as idMarque , marqueMoto.nom as nomMarque
from categorieMoto
join modele
on categorieMoto.id = modele.idCategorie
join marqueMoto
on modele.idMarque = marqueMoto.id;